# CHANGELOG



## v0.2.3 (2024-04-07)

### Fix

* fix(deploy): 3rd try rerame to my_package ([`b18308d`](https://gitlab.com/augustoitrat/my-package/-/commit/b18308dba7735f0a2640be032e2d035a2f615a70))

### Unknown

* change folder name ([`a7060bf`](https://gitlab.com/augustoitrat/my-package/-/commit/a7060bfa32657343ee4dd8da057bef430f1f6a6f))


## v0.2.2 (2024-04-07)

### Fix

* fix(deploy): publish ([`d34ae9e`](https://gitlab.com/augustoitrat/my-package/-/commit/d34ae9e70cbf0d9d6246897dee8e1c3165969356))


## v0.2.1 (2024-04-07)

### Fix

* fix(deploy): add deploy pipeline ([`d471c2d`](https://gitlab.com/augustoitrat/my-package/-/commit/d471c2dc3e5596c9889354abb9d27977182431a1))

### Unknown

* add an __init__.py file into my-package ([`0662f8d`](https://gitlab.com/augustoitrat/my-package/-/commit/0662f8d4db8afd593119957fb35b39bf1e62591e))

* New directory ([`e53b929`](https://gitlab.com/augustoitrat/my-package/-/commit/e53b929dc83e23660e5b01ba8c64dd71bf889372))


## v0.2.0 (2024-04-07)

### Feature

* feat(test): add new test module ([`0de887a`](https://gitlab.com/augustoitrat/my-package/-/commit/0de887ad03d92bac564fa800e35bc396d870d896))


## v0.1.0 (2024-04-07)

### Feature

* feat(semantic-release): add semantic release to the repository ([`e5159ee`](https://gitlab.com/augustoitrat/my-package/-/commit/e5159ee584afc0086f1f3fc3df7c893a27517234))


## v0.0.0 (2024-04-07)

### Unknown

* Update .gitlab-ci.yml - Setup Gitlab for Continuous Deployment ([`b5dba13`](https://gitlab.com/augustoitrat/my-package/-/commit/b5dba1303ad61a00f9d977dc6c715a1101e46ad4))

* Update pyproject.toml - Configure Python Semantic Release ([`792d338`](https://gitlab.com/augustoitrat/my-package/-/commit/792d338ee9cfb67202cc27577543e63dbede38d1))

* Add pyproject.toml to init poetry ([`2d2fec1`](https://gitlab.com/augustoitrat/my-package/-/commit/2d2fec153e05cb250a04da6bb6e2977754f5218d))

* Update .gitlab-ci.yml ([`7eca551`](https://gitlab.com/augustoitrat/my-package/-/commit/7eca551ae868a990bd6d9430febf8eeebfe3196e))

* Add new file ([`71b2098`](https://gitlab.com/augustoitrat/my-package/-/commit/71b209899898aed3507a6783ef839bb9e38902ed))

* Initial commit ([`c1530f5`](https://gitlab.com/augustoitrat/my-package/-/commit/c1530f56a75cc2f199ca1f1c4af5eed651d109bb))
